;(function($) { 
	if($('.js-slider').length) {
		$('.js-slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
				$('.js-slider').removeClass('is-loading');
			})
		$('.js-slider').slick({
			arrows: false
		});
	}
	if($('.js-lightbox').length) {
		lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
	}

	
}($));
	
