;(function() {
if($('.js-nav').length) {
		var $trigger = $('.js-nav-trigger'),
				$content = $('.js-nav-content'),
				$container = $('.js-nav');
		$trigger.on('click', function() {
			$(this).closest($container).find($content).toggleClass('is-visible');
			$(this).toggleClass('is-active');
			$(this).closest('html').toggleClass('is-locked');
		})
	};
}())